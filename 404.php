<?php
/**
 * Template Name: Full-width ( no sidebar )
 * 
 * This is the template that displays full width page without sidebar
 * 
 * @package flirt
 */

get_header(); ?>

<div id="site-content">
	<div class="container">
		<div class="row clearfix">
			
			<div class="col-lg-12">
				
				<section class="text-center">
					<h3>404 Page Not Found</h3>
					<hr/>
					<p>We are sorry but the page you are looking for does not exists.</p>
                </section>
                
			</div>
			
		</div>
	</div>
</div>

<?php get_footer(); ?>