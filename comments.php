<?php
/**
 * The template for displaying Comments
 * 
 * @package flirt
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="blog-post-comments">
	<?php if ( have_comments() ) : ?>
	<h4 class="post-comments-title">Comments</h4>
	
	<div class="row">
		<div class="col-md-8 col-md-offset-4 col-sm-12">
	
			<ol id="comments" class="commentlist" >
				<?php
					/* Loop through and list the comments. Tell wp_list_comments()
					 * to use flirt_comment() to format the comments.
					 * If you want to override this in a child theme, then you can
					 * define flirt_comment() and that will be used instead.
					 * See flirt_comment() in inc/template-tags.php for more.
					 */
					wp_list_comments( array ( 
						'callback' => 'flirt_comment',
						'avatar_size' => 45
						)
					);
				?>
			</ol>
			
		</div>
	</div>
	
	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
	<nav class="pagination">
		<?php 
			paginate_comments_links( array (
				'prev_text' => 'Older',
				'next_text' => 'Newer',
				'add_fragment' => '#blog-post-comments'
				) 
			); 
		?>
	</nav>
	<?php endif; ?>
	
	<?php else: ?>
	<h4 class="post-comments-title">No Comments</h4>
	
	<?php endif; // have_comments() ?>
	
	<?php
	$fields =  array (
		'author' => 
			'<div class="author-info row">'.
			'<div class="col-sm-4">' . '<label for="author">' . __( 'Name' ) . ( $req ? '&nbsp;<span class="required"><i class="icon-asterisk"></i></span>' : '' ) . '</label> ' .
			'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></div>',
			
		'email'  => 
			'<div class="col-sm-4">' . '<label for="email">' . __( 'Email' ) . ( $req ? '&nbsp;<span class="required"><i class="icon-asterisk"></i></span>' : '' ) . '</label> ' .
			'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></div>',
			
		'url' => 
			'<div class="col-sm-4">' . '<label for="url">' . __( 'Website' ) . '</label> ' .
			'<input id="url" name="url" type="text" value="' . esc_attr(  $commenter['comment_author_url'] ) . '" size="30"' . $aria_req . ' /></div>'.
			'</div>'
		
	);
	
	$comments_args = array (
		'title_reply' =>  __( '<h4 class="post-comments-title">Leave a Reply</h4>' ),
		'title_reply_to' => __( 'to %s' ),
		'cancel_reply_link' => __( '<h5>Click here to cancel a reply</h5>' ),
	    'fields' =>  $fields,
	    
	    'comment_field' =>
	    	'<div class="author-info">'.
				'<label for="author-comment">' . __( 'Comment' ) . '&nbsp;<span class="required"><i class="icon-asterisk"></i></span></label>'.
				'<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>'.
			'</div>',
			
		'comment_notes_before' => '<p class="respond-note">' . __( 'Your email address will not be published.' ) . ( $req ? __( ' Required fields are marked <span class="required"><i class="icon-asterisk"></i></span>' ) : '' ) . '</p>',
		'comment_notes_after' => '',
		
		'label_submit' => __( 'Post Comment' ),
		'id_submit' => 'reply-submit',
	);
	
	comment_form( $comments_args ); ?>
	
</div>