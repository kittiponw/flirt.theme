<?php
/**
 * @package flirt
 */
?>
<article id="page-<?php the_ID(); ?>" <?php post_class( "page-default" ); ?>>
	<?php the_content(); ?>
</article>