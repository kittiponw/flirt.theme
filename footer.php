<?php
/**
 * The template for displaying the footer.
 *
 * @package flirt
 */
?>

<footer id="site-footer">
	<div class="container">
		<h6 id="site-footer-text" class="pull-left">WordPress Theme Designed & Developed by Kittiponw</h6>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>