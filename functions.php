<?php
/**
 * Functions and Definitions
 * 
 * @package flirt
 */

/**
 * Sets up the content width value ( pixels ) based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640;


if ( ! function_exists( 'flirt_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function flirt_setup() {
	
	// Make theme available for translation
	load_theme_textdomain( 'flirt', get_template_directory() . '/languages' );
	
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	
	// Enable support for Post Thumbnails on posts and pages.
	add_theme_support( 'post-thumbnails' );
	
	add_image_size( 'featured-small', 200, 250, true );
	
	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array(
		    // 'audio', 'gallery', 'image', 'quote', 'status', 'video'
		    'image', 'gallery', 'audio', 'video'
		) );
		
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'flirt' )
	) );
	
}
endif; // flirt_setup
add_action( 'after_setup_theme', 'flirt_setup' );


/**
 * Register widget area and update sidebar with default widgets.
 */
function flirt_widgets_init () {
	
	register_sidebar( array (
		'id' 			=> 'sidebar-1',
		'name' 			=> __( 'Sidebar', 'flirt' ),
		'description' 	=> __( 'Appears on Posts', 'flirt' ),
		'before_widget'	=> '<aside id="%1$s" class="widget-module">',
		'after_widget' 	=> '</aside>',
		'before_title'	=> '<h4 class="widget-title">',
		'after_title' 	=> '</h4>',
	) );
	
}
add_action( 'widgets_init', 'flirt_widgets_init' );


/**
 * Enqueue scripts and styles
 */
function flirt_scripts () {
	
	// css
	wp_enqueue_style( 'flirt-bootstrap-css', 
		get_template_directory_uri() . '/supplement/bootstrap/css/bootstrap.min.css',
		array(), 'v3.2.0', false
	);
	wp_enqueue_style( 'flirt-bootstrap_theme-css', 
		get_template_directory_uri() . '/supplement/bootstrap/css/bootstrap-theme.min.css',
		array(), 'v3.2.0', false
	);
	wp_enqueue_style( 'flirt-font_awesome-css', 
		get_template_directory_uri() . '/supplement/font-awesome/css/font-awesome.min.css',
		array(), 'v3.2.1', false
	);
	wp_enqueue_style( 'flirt-style', get_stylesheet_uri() );
	
	// js
	wp_enqueue_script( 'flirt-jquery',
		get_template_directory_uri() . '/supplement/jquery/jquery.min.js',
		array(), 'v1.11.1', true
	);
	wp_enqueue_script( 'flirt-bootstrap-js',
		get_template_directory_uri() . '/supplement/bootstrap/js/bootstrap.min.js',
		array( 'jquery' ), 'v3.2.0', true
	);
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) :
		wp_enqueue_script( 'comment-reply' );
		wp_enqueue_script( 'flirt-jquery-validation',
			get_template_directory_uri() . '/supplement/jquery/jquery.validate.min.js',
			array( 'jquery' ), 'v1.11.1', true
		);
		wp_enqueue_script( 'flirt-comment-validator',
			get_template_directory_uri() . '/inc/js/comment-validator.js',
			array( 'jquery' ), null, true
		);
	endif;
	
	wp_enqueue_script( 'flirt-functions',
		get_template_directory_uri() . '/inc/js/theme.js',
		array( 'jquery' ), null, true
	);
	
}
add_action( 'wp_enqueue_scripts', 'flirt_scripts' );


/**
 * Add HTML5 shiv and Respond.js for IE8 support of HTML5 elements and media queries
 */
function flirt_ie_support_header () {
    echo '<!--[if lt IE 9]>'. "\n";
    echo '<script src="' . esc_url( get_template_directory_uri() . '/js/html5shiv.min.js' ) . '"></script>'. "\n";
    echo '<script src="' . esc_url( get_template_directory_uri() . '/js/respond.min.js' ) . '"></script>'. "\n";
    echo '<![endif]-->'. "\n";
}
// add_action( 'wp_head', 'flirt_ie_support_header' );

remove_action( 'set_comment_cookies', 'wp_set_comment_cookies' );

remove_filter( 'the_content', 'wpautop' );

// Load custom nav walker
require get_template_directory() . '/inc/navwalker.php';

// Load custom template tags for this theme
require get_template_directory() . '/inc/template-tags.php';

// Load custom shortcodes for this theme
require get_template_directory() . '/inc/shortcodes.php';

// Load custom meta box for this theme
require get_template_directory() . '/inc/metabox/metabox.php';
