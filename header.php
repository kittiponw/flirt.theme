<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package flirt
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>
	
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
	
	<?php wp_head(); ?>
	
	<?php if ( is_admin_bar_showing() ) : ?>
		<style>
			body.admin-bar #site-header {
				top: 32px;
			}
			@media screen and ( max-width: 782px ) {
				body.admin-bar #site-header {
					top: 46px !important;
				}
			}
			@media (max-width: 600px) { 
				#wpadminbar { position: fixed; }
			}
		</style>
	<?php endif; ?>
</head>

<body <?php body_class(); ?>>
	<header id="site-header" class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			
			<div id="site-branding" class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					 <span class="sr-only">Toggle navigation</span>
					 <span class="icon-bar"></span>
					 <span class="icon-bar"></span>
					 <span class="icon-bar"></span>
				</button>
				<div id="site-branding-body">
					<?php if ( get_theme_mod( 'site_logo' ) ) : ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img id="site-branding-logo" src="<?php echo get_theme_mod( 'site_logo' ); ?>" /></a>
					
					<?php else: ?>
					<hgroup>
						<h1 id="site-branding-name"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
						<h6 id="site-branding-desc"><?php bloginfo( 'description' ); ?></h6>
					</hgroup>
					
					<?php endif; ?>
				</div>
			</div>
			
			<?php
			
			$defaults  = array(
				'theme_location' 	=> 'primary',
				'container' 		=> 'div',
				'container_id' 		=> 'site-navigation',
				'container_class'	=> 'navbar-collapse collapse',
				'menu_id' 			=> 'site-navigation-top',
				'menu_class' 		=> 'nav navbar-nav navbar-right',
				'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
				'walker' 			=> new wp_bootstrap_navwalker()
			);
			
			wp_nav_menu( $defaults ); ?>
			
		</div>
	</header>
	
	<div id="page-title">
		<div class="container">
			<h4 id="page-title-name" class="pull-left">
				<?php
					$page_title = empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent );
					$page_title = strtoupper( $page_title );
					
					echo $page_title;
				?>
			</h4>
			<?php flirt_breadcrumb(); ?>
		</div>
	</div>
