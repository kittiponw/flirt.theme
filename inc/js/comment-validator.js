$( '#commentform' ).validate( {
	focusInvalid: false,
	errorPlacement: function( error, element ) {},
	rules: {
        author: {
        	required: true
        },
        email: {
            required: true,
            email: true
        },
        comment: {
        	required: true
        }
    }
} );