var margin_top = 85;
var phone_width = 767;


function toggleSiteHeaderCollapse() {
	if ( $( "#site-header" ).offset().top > 70) {
		
		if( $( window ).width() >= phone_width ) {
			$( "#site-header" ).addClass( "header-collapsed" );
		}
        
    } else {
    	$( "#site-header" ).removeClass( "header-collapsed" );
        
    }
}

$( window ).scroll(
	toggleSiteHeaderCollapse
);
toggleSiteHeaderCollapse();


function parallaxBackgroundPosition() {
	
	var heightWindow = $(window).height();
	var topWindow = $(window).scrollTop() - margin_top;
	var bottomWindow = topWindow + heightWindow;
	var currentWindow = (topWindow + bottomWindow) / 2;
	
	$( ".parallax-bg" ).each( function() {
		
		var obj = $(this);
		var height = obj.height();
		var top = obj.offset().top - margin_top;
		var bottom = ( top + height ) + margin_top;
		
		// WHEN IN RANGE
		if( top < bottomWindow && bottom > topWindow ) {
	        var value = -( currentWindow - top ) / 2;
	        obj.css( "background-position", "50% " + value + "px");
		}
		
	} );
}

$( window ).scroll(
	parallaxBackgroundPosition
);
parallaxBackgroundPosition();

$( "[data-toggle='tooltip']" ).tooltip();



/** Customize theme unordered list **/
$( 'div[class^="list-style"]' ).each( function() {
	
	var obj = $( this );
	obj.find( 'ul' ).addClass( 'icons-ul' );
	
	var bullet_type = obj.attr('class').split( '_' )[1];
	obj.find( 'li' ).prepend( '<i class="icon-li ' + bullet_type + '"></i>' );
});