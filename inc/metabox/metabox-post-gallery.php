<?php

$post_gallery_metabox = array (
	'id' 		=> 'post-gallery-format-meta-box',
	'title'		=> 'Flirt Post Gallery',
	'callback'	=> 'flirt_post_gallery_metabox_callback',
	'context'	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array (
	
		array (
			'id' 	=> 'image-ids',
            'label' => 'Add Gallery',
            'type' 	=> 'gallery'
        ),
        
	 )
);

function flirt_metabox_post_gallery_init () {
	
	global $post_gallery_metabox;
	
	foreach ( array( 'post' ) as $screen ) {
		add_meta_box(
			$post_gallery_metabox[ 'id' ],
			$post_gallery_metabox[ 'title' ],
			$post_gallery_metabox[ 'callback' ],
			$screen,
			$post_gallery_metabox[ 'context' ],
			$post_gallery_metabox[ 'priority' ]
		);
	}
}
add_action( 'add_meta_boxes', 'flirt_metabox_post_gallery_init' );

function flirt_post_gallery_metabox_callback ( $post ) {
	
	global $post_gallery_metabox;
	
	echo '<div class="meta">';
	echo '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum.</p>';
	echo '<div class="wrapper">';
	
	foreach ( $post_gallery_metabox[ 'fields' ] as $f ) {
		
		$meta = get_post_meta( $post->ID, $f[ 'id' ], true );
		
		switch ( $f[ 'type' ] ) {
			case 'gallery':
				
				echo '<div id="preview-wrapper">';
				
				if ( $meta ) {
					
					echo '<ul id="gallery-preview">';
					
					foreach( explode( ",", $meta ) as $key => $image_id ) {
						$image_id = trim( $image_id );
						echo '<li><span class="order">'.($key + 1).'</span><img src="'.wp_get_attachment_url( $image_id ).'" /></li>';
					}
					
					echo '</ul>';
					
				}
				
				echo '</div>';
				
				echo 
				'<p>'.
					'<span id="gallery-editor"><a href="#">'.( !$meta ? "Add Gallery" : "Edit Gallery" ).'</a></span>'.
					'<span id="gallery-remover" style="display:'.( $meta ? "block" : "none" ).';"><a href="#">Remove Gallery</a></span>'.
				'</p>'.
				'<input type="hidden" id="'. $f['id'] .'" name="'. $f['id'] .'" value="'. $meta .'"/>';
				
				break;
		}
		
	}
	
	echo '</div>'; // .post-meta
	echo '</div>'; // .meta
	
	echo '<input type="hidden" name="flirt_post_gallery_nonce" value="', wp_create_nonce( basename( __FILE__ ) ), '" />';
	
}

function flirt_save_gallery_meta ( $post_id ) {
	
	global $post_gallery_metabox;
	
	// verify nonce
	if ( ! wp_verify_nonce( $_POST[ 'flirt_post_gallery_nonce' ], basename(__FILE__) ) ) {
        return $post_id;
    }
	
	// check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }
	
	// check permissions
    if ( 'page' == $_POST[ 'post_type' ] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return $post_id;
        }
		
    } elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
		
    }
	
	foreach ( $post_gallery_metabox[ 'fields' ] as $f ) {
		
		$old = get_post_meta( $post_id, $f[ 'id' ], true );
        $new = $_POST[ $f[ 'id' ] ];
		
		if ( $new && $new != $old ) {
            update_post_meta( $post_id, $f[ 'id' ], $new );
				
        } elseif ( '' == $new && $old ) {
            delete_post_meta( $post_id, $f[ 'id' ], $old );
            
        }
	}
	
}
add_action( 'save_post', 'flirt_save_gallery_meta' );