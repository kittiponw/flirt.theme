<?php

$post_image_metabox = array ( 
	'id' 		=> 'post-image-format-meta-box',
	'title'		=> 'Flirt Post Image',
	'callback'	=> 'flirt_post_image_metabox_callback',
	'context'	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array (
	
		array (
			'id' 	=> 'image-id',
            'label' => 'Set Post Image',
            'type' 	=> 'image'
        ),
        
	 )
);

function flirt_metabox_post_image_init () {
	
	global $post_image_metabox;
	
	foreach ( array( 'post' ) as $screen ) {
		add_meta_box(
			$post_image_metabox[ 'id' ],
			$post_image_metabox[ 'title' ],
			$post_image_metabox[ 'callback' ],
			$screen,
			$post_image_metabox[ 'context' ],
			$post_image_metabox[ 'priority' ]
		);
	}
		
}
add_action( 'add_meta_boxes', 'flirt_metabox_post_image_init' );

function flirt_post_image_metabox_callback ( $post ) {
	
	global $post_image_metabox;
	
	echo '<div class="meta">';
	echo '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum.</p>';
	echo '<div class="wrapper">';
	
	foreach ( $post_image_metabox[ 'fields' ] as $f ) {
		
		$meta = get_post_meta( $post->ID, $f[ 'id' ], true );
		
		switch ( $f[ 'type' ] ) {
			case 'image':
				
				// '<p id="post-image-preview" style="display:'.( $meta ? "block" : "none" ).';">'.
				echo '<div id="image-preview">';
				if ( $meta ) {
					echo '<a href="#" class="image-chooser">';
					echo '<img src="'.wp_get_attachment_url( $meta ).'" />';
					echo '</a>';
				}
				
				echo '</div>';
				
				echo 
				'<p>'.
					'<span id="image-setter" style="display:'.( !$meta ? "block" : "none" ).';"><a href="#" class="image-chooser">'.$f['label'].'</a></span>'.
					'<span id="image-remover" style="display:'.( $meta ? "block" : "none" ).';"><a href="#">Delete post image</a></span>'.
				'</p>'.
				'<input type="hidden" id="'. $f['id'] .'" name="'. $f['id'] .'" value="'. $meta .'"/>';
				
				break;
		}
		
	}
	
	echo '</div>'; // .post-meta
	echo '</div>'; // .meta
	
	echo '<input type="hidden" name="flirt_post_image_nonce" value="', wp_create_nonce( basename( __FILE__ ) ), '" />';
	
}

function flirt_save_image_meta ( $post_id ) {
	
	global $post_image_metabox;
	
	// verify nonce
	if ( ! wp_verify_nonce( $_POST[ 'flirt_post_image_nonce' ], basename(__FILE__) ) ) {
        return $post_id;
    }
	
	// check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }
	
	// check permissions
    if ( 'page' == $_POST[ 'post_type' ] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return $post_id;
        }
		
    } elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
		
    }
	
	foreach ( $post_image_metabox[ 'fields' ] as $f ) {
		
		$old = get_post_meta( $post_id, $f[ 'id' ], true );
        $new = $_POST[ $f[ 'id' ] ];
		
		if ( $new && $new != $old ) {
            update_post_meta( $post_id, $f[ 'id' ], $new );
				
        } elseif ( '' == $new && $old ) {
            delete_post_meta( $post_id, $f[ 'id' ], $old );
            
        }
	}
	
}
add_action( 'save_post', 'flirt_save_image_meta' );
