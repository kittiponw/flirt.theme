<?php

$post_video_metabox = array (
	'id' 		=> 'post-video-format-meta-box',
	'title'		=> 'Flirt Post Video',
	'callback'	=> 'flirt_post_video_metabox_callback',
	'context'	=> 'normal',
	'priority' 	=> 'high',
	'fields' 	=> array (
	 
	 	array (
			'id' 	=> 'video-url',
            'label' => 'Video URL',
            'type' 	=> 'text'
        ),
        
		array (
			'id' 	=> 'video-embedded',
            'label'	=> 'Embedded',
            'type' 	=> 'checkbox'
        ),
        
	 )
);

function flirt_metabox_post_video_init () {
	
	global $post_video_metabox;
	
	foreach ( array( 'post' ) as $screen ) {
		add_meta_box(
			$post_video_metabox[ 'id' ],
			$post_video_metabox[ 'title' ],
			$post_video_metabox[ 'callback' ],
			$screen,
			$post_video_metabox[ 'context' ],
			$post_video_metabox[ 'priority' ]
		);
	}
}
add_action( 'add_meta_boxes', 'flirt_metabox_post_video_init' );

function flirt_post_video_metabox_callback ( $post ) {
	
	global $post_video_metabox;
	
	echo '<div class="meta">';
	echo '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum.</p>';
	echo '<div class="wrapper">';
	
	foreach ( $post_video_metabox[ 'fields' ] as $f ) {
		
		$meta = get_post_meta( $post->ID, $f[ 'id' ], true );
		
		switch ( $f[ 'type' ] ) {
			case 'text':
				
				echo '<label for="', $f[ 'id' ], '">', $f[ 'label' ], '</label>',
				'<input type="text" id="', $f[ 'id' ], '" name="', $f[ 'id' ], '" value="', $meta ,'" placeholder="Example: http://www.youtube.com/watch?v=video-id" />';
				
				break;
				
			case 'checkbox':
				echo '<label class="selectit post-media-embed" for="', $f[ 'id' ], '">',
				'<input type="checkbox" id="', $f[ 'id' ], '" name="', $f[ 'id' ], '" ', ( $meta ? 'checked="checked"' : '' ), '/>',
				 $f[ 'label' ],'</label>';
				break;
		}
		
	}
	echo '</div>'; // .post-media
	echo '</div>'; // .meta
	
	// Use nonce for verification
    echo '<input type="hidden" name="flirt_post_video_nonce" value="', wp_create_nonce( basename( __FILE__ ) ), '" />';
}

function flirt_save_video_meta ( $post_id ) {
	
	global $post_video_metabox;
	
	// verify nonce
	if ( ! wp_verify_nonce( $_POST[ 'flirt_post_video_nonce' ], basename(__FILE__) ) ) {
        return $post_id;
    }
	
	// check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }
	
	// check permissions
    if ( 'page' == $_POST[ 'post_type' ] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return $post_id;
        }
		
    } elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
		
    }
	
	foreach ( $post_video_metabox[ 'fields' ] as $f ) {
		
		$old = get_post_meta( $post_id, $f[ 'id' ], true );
        $new = $_POST[ $f[ 'id' ] ];
		
		if ( $new && $new != $old ) {
            update_post_meta( $post_id, $f[ 'id' ], $new );
				
        } elseif ( '' == $new && $old ) {
            delete_post_meta( $post_id, $f[ 'id' ], $old );
            
        }
	}
	
}
add_action( 'save_post', 'flirt_save_video_meta' );
