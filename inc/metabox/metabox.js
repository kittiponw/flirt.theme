var meta_image_frame;

jQuery( document ).ready( function( $ ) {
	
	postbox_toggle();
	
	$( "input.post-format" ).change( function() {
		postbox_toggle();
	} );
	
} );

function postbox_toggle () {
	
	jQuery( "#post-image-format-meta-box" ).hide();
	jQuery( "#post-gallery-format-meta-box" ).hide();
	jQuery( "#post-audio-format-meta-box" ).hide();
	jQuery( "#post-video-format-meta-box" ).hide();
	
	var post_format = jQuery( 'input.post-format:checked' ).val();
	
	switch ( post_format ) {
		case 'image' : jQuery( '#post-image-format-meta-box' ).show(); break;
		case 'gallery' : jQuery( '#post-gallery-format-meta-box' ).show(); break;
		case 'audio': jQuery( "#post-audio-format-meta-box" ).show(); break;
		case 'video': jQuery( "#post-video-format-meta-box" ).show(); break;
	}
}

// POST IMAGE
jQuery( '.image-chooser' ).live( 'click' , function( event ) {
	
	event.preventDefault();
	
	// Create the media frame.
	meta_image_frame = wp.media.frames.meta_image_frame = wp.media( {
		title: "Select Post Image", 
	 	button: { 
	 		text: jQuery( this ).data( 'uploader_button_text' ),
	 	},
	 	
	 	multiple: false  // Set to true to allow multiple files to be selected
	 	
	} );
	 
	// When frame is opened, run a callback
	meta_image_frame.on( 'open', function() {
		
		if ( jQuery( '#image-id' ).val().length ) {
	 		var selection = meta_image_frame.state().get( 'selection' );
	 		var attachment = wp.media.attachment( jQuery( '#image-id' ).val() );
	 		
	 		selection.add( attachment ? [ attachment ] :  [] );
	 	}
	});
	 
	 
	// When an image is selected, run a callback.
	meta_image_frame.on( 'select', function() {
	 	// We set multiple to false so only get one image from the uploader
	 	var attachment = meta_image_frame.state().get( 'selection' ).first().toJSON();
	 	
	 	jQuery( '#image-id' ).val( attachment.id );
	 	// jQuery( '#post-image-preview' ).find( 'img' )
	 	// jQuery( '#post-image-preview' ).show();
	 	
	 	var preview_code= '<a href="#" class="image-chooser">';
	 	preview_code += '<img src="' + attachment.url + '" />';
	 	preview_code += '</a>';
	 	
	 	jQuery( '#image-preview' ).empty();
	 	jQuery( '#image-preview' ).append( preview_code );
	 	
	 	jQuery( '#image-remover' ).show();
	 	jQuery( '#image-setter' ).hide();
	 	
	} );
	 
	 // Finally, open the modal
	meta_image_frame.open();
	 
} );

jQuery( '#image-remover' ).find( 'a' ).live( 'click' , function( event ) {
	
	event.preventDefault();
	
	jQuery( '#image-id' ).removeAttr( 'value' );
	jQuery( '#image-preview' ).empty();
	jQuery( '#image-remover' ).hide();
	jQuery( '#image-setter' ).show();
	
} );

// POST GALLERY
jQuery( '#gallery-editor' ).find( 'a' ).live( 'click' , function( event ) {
	
	event.preventDefault();
	 
	 // Create the media frame.
	 meta_image_frame = wp.media.frames.meta_image_frame = wp.media( {
	 	title: "Create/Edit Gallery", 
	 	button: { 
	 		text: jQuery( this ).data( 'uploader_button_text' ),
	 	},
	 	
	 	multiple: true  // Set to true to allow multiple files to be selected
	 	
	 } );
	 
	 // When frame is opened, run a callback
	 meta_image_frame.on( 'open', function() {
	 	
	 	if ( jQuery( '#image-ids' ).val().length ) {
	 		var selection = meta_image_frame.state().get( 'selection' );
	 		
	 		var ids = jQuery( '#image-ids' ).val().split(',');
	 		ids.forEach( function( id ) {
		 		attachment = wp.media.attachment( id );
		 		attachment.fetch();
		 		selection.add( attachment ? [ attachment ] : [] );
		 	});
	 	}
	 	
	 });
	 
	 // When an image is selected, run a callback.
	 meta_image_frame.on( 'select', function() {
	 	
	 	var selection = meta_image_frame.state().get( 'selection' );
	 	var image_ids = [];
	 	var preview_code= '<ul id="gallery-preview">';
	 	
	 	selection.map( function( attachment, i ) {
	 		var attachment = attachment.toJSON();
	 		preview_code += '<li><span class="order">' + (++i) + '</span><img src="' + attachment.url + '" /></li>';
	 		
	 		image_ids.push( attachment.id );
	 	});
	 	
	 	preview_code += '</ul>';
	 	
	 	jQuery( '#preview-wrapper' ).empty();
	 	jQuery( '#preview-wrapper' ).append( preview_code );
	 	
	 	jQuery( '#image-ids' ).val( image_ids.join( "," ) );
	 	jQuery( '#gallery-editor' ).find( 'a' ).text( "Edit Gallery" );
	 	jQuery( '#gallery-remover' ).show();
	 	
	 } );
	 
	 // Finally, open the modal
	 meta_image_frame.open();
	 
} );

jQuery( '#gallery-remover' ).find( 'a' ).live( 'click' , function( event ) {
	
	event.preventDefault();

	jQuery( '#image-ids' ).removeAttr( 'value' );	
	jQuery( '#preview-wrapper' ).empty();
 	jQuery( '#gallery-editor' ).find( 'a' ).text( "Add Gallery" );
 	jQuery( '#gallery-remover' ).hide();
	
} );