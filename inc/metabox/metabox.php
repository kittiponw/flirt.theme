<?php

function flirt_metabox_style(){
    global $typenow;
    if( $typenow == 'post' ) {
        wp_enqueue_style( 'flirt-meta-style', get_template_directory_uri() . '/inc/metabox/metabox.css' );
		wp_enqueue_style( 'thickbox' );
    }
}
add_action( 'admin_print_styles', 'flirt_metabox_style' );

function flirt_metabox_script () {
	global $typenow;
    if( $typenow == 'post' ) {
        wp_enqueue_script( 'flirt-meta-script', get_template_directory_uri() . '/inc/metabox/metabox.js' );
    }
}
add_action( 'admin_print_scripts', 'flirt_metabox_script' );

// META FOR POST-IMAGE FORMAT
require get_template_directory() . '/inc/metabox/metabox-post-image.php';


// META FOR POST-GALLERY FORMAT
require get_template_directory() . '/inc/metabox/metabox-post-gallery.php';


// META FOR POST-AUDIO FORMAT
require get_template_directory() . '/inc/metabox/metabox-post-audio.php';


// META FOR POST-VIDEO FORMAT
require get_template_directory() . '/inc/metabox/metabox-post-video.php';


// MOVE THE AUTHOR METABOX INTO THE PUBLISH METABOX
add_action( 'admin_menu', 'remove_author_metabox' );
add_action( 'post_submitbox_misc_actions', 'move_author_to_publish_metabox' );
function remove_author_metabox() {
    remove_meta_box( 'authordiv', 'post', 'normal' );
}
function move_author_to_publish_metabox() {
    global $post_ID;
    $post = get_post( $post_ID );
    echo '<div id="author" class="misc-pub-section" style="border-top-style:solid; border-top-width:1px; border-top-color:#EEEEEE; border-bottom-width:0px;">Author: ';
    post_author_meta_box( $post );
    echo '</div>';
}