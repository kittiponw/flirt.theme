<?php

add_shortcode( 'post_media', function( $atts, $content = null ) {
	return '<div class="post-media">'.do_shortcode( $content ).'</div>';
} );

add_shortcode( 'post_content', function( $atts, $content = null ) {
	return '<div class="post-content">'.do_shortcode( $content ).'</div>';
} );

/** Imaage shortcodes **/
add_shortcode( 'image', function( $atts, $content = null ) {
	return '<img src="http://localhost/flirt/wp-content/uploads/sites/3/2014/08/9x4_unsplash_by_aleks_dorohovich.jpg" class="img-responsive" />';
} );

/** HTML codes **/
add_shortcode( 'p', function( $atts, $content = null ) {
	return '<p>'.do_shortcode( $content ).'</p>';
} );

add_shortcode( 'blockquote', function( $atts, $content = null ) {
	return '<blockquote>'.do_shortcode( $content ).'</blockquote>';
} );


/** CONTENT SECTION **/

add_shortcode ( 'section', function ( $atts, $content = null ) {
	
	extract ( shortcode_atts ( array(
		'id' => '',
		'class' => '',
		'css' => ''
	), $atts ) );
	
	$shortcode_attrs = array();
	
	if ( ! empty( $id ) ) {
		$shortcode_attrs[] = 'id="'.$id.'"';
	}
	
	if ( ! empty( $class ) ) {
		$shortcode_attrs[] = 'class="'.$class.'"';
	}
	
	if ( ! empty( $css ) ) {
		$shortcode_attrs[] = 'style="'.$css.'"';
	}
	
	return '<section '.implode( " ", $shortcode_attrs ).'>'.do_shortcode( $content ).'</section>';

} );

/** GRID SYSTEM **/

add_shortcode( 'row', function( $atts, $content = null ) {
	extract ( shortcode_atts ( array(
		'class' 	=> '',
		'css' 		=> '',
	), $atts ) );
	
	$shortcode_attrs = array();
	
	$classes[] = "row";
	if ( ! empty( $class ) ) { $classes[] = $class; }
	if ( ! empty( $classes ) ) { $shortcode_attrs[] = 'class="'.implode( " ", $classes ).'"'; }
	if ( ! empty( $css ) ) { $shortcode_attrs[] = 'style="'.$css.'"'; }

	return '<div '.implode( " ", $shortcode_attrs ).'>' .do_shortcode( $content ). '</div>';
	
} );
add_shortcode( 'column', function( $atts, $content = null ) {
	extract ( shortcode_atts ( array(
	
		/** Column Size**/
		'lg' => '', 'md' => '', 'sm' => '', 'xs' => '',
		
		/** Offsetting Columns :: Move columns to the right using .col-[lg,md,sm,xs]-offset-* classes **/
		'lg_offset' => '', 'md_offset' => '', 'sm_offset' => '', 'xs_offset' => '',
		
		/** Column Ordering :: Easily change the order of our built-in grid columns **/
		'lg_push' => '', 'md_push' => '', 'sm_push' => '', 'xs_push' => '',
		'lg_pull' => '', 'md_pull' => '', 'sm_pull' => '', 'xs_pull' => '',
		
		'class' => '',
		'css' => ''
	
	), $atts ) );
	
	if( is_array( $atts ) ) {
		foreach ( $atts as $key => $value ) {
			$classes[] = str_replace( '_', '-', 'col-'.$key.'-'.$value );
		}
	}
	
	if ( ! empty( $class ) ) { $classes[] = $class; }
	if ( ! empty( $classes ) ) { $shortcode_attrs[] = 'class="'.implode( " ", $classes ).'"'; }
	if ( ! empty( $css ) ) { $shortcode_attrs[] = 'style="'.$css.'"'; }
	
	return '<div '.implode( " ", $shortcode_attrs ).'>' .do_shortcode( $content ). '</div>';
	
} );

add_shortcode( 'ul_list', function( $atts, $content = null ) {
	
	extract ( shortcode_atts ( array(
		'bullet' => ''
	), $atts ) );
	
	return '<div class="list-style_'.$bullet.'">' .do_shortcode( $content ). '</div>';
});

/** TYPOGRAPHY **/

add_shortcode( 'dropcap', function( $atts, $content = null ) {
	
	extract ( shortcode_atts ( array(
		'class' => '',
		'css' => ''
	), $atts ) );
	
	$shortcode_attrs = array();
	
	$classes[] = "dropcap";
	if ( ! empty( $class ) ) { $classes[] = $class; }
	if ( ! empty( $classes ) ) { $shortcode_attrs[] = 'class="'.implode( " ", $classes ).'"'; }
	if ( ! empty( $css ) ) { $shortcode_attrs[] = 'style="'.$css.'"'; }
	
	return '<div '.implode( " ", $shortcode_attrs ).'>'.do_shortcode( $content ).'</div>';
	
} );

add_shortcode( 'heading', function( $atts, $content = null ) {
	
	extract ( shortcode_atts ( array(
		'type' => 'h3',
		'class' => '',
		'css' => ''
	), $atts ) );
	
	$shortcode_attrs = array();
	
	if ( ! empty( $class ) ) {
		$shortcode_attrs[] = 'class="'.$class.'"';
	}
	
	if ( ! empty( $css ) ) {
		$shortcode_attrs[] = 'style="'.$css.'"';
	}
	
	return '<'.$type.' '.implode( " ", $shortcode_attrs ).'>' .do_shortcode( $content ). '</'.$type.'>';
	
} );

add_shortcode( 'blockquote', function( $atts, $content = null ) {
	extract ( shortcode_atts ( array(
		'align' 		=> 'left',
		'class' 		=> '',
		'css' 			=> ''
	), $atts ) );
	
	$shortcode_attrs = array();
	
	$possible_align = array( "left", "right" );
	if ( in_array( $align, $possible_align ) ) { $classes[] = ( $align == "right" ? "blockquote-reverse" : "" ); }
	
	if ( ! empty( $class ) ) { $classes[] = 'class="'.$class.'"'; }
	if ( ! empty( $classes ) ) { $shortcode_attrs[] = 'class="'.implode( " ", $classes ).'"'; }
	if ( ! empty( $css ) ) { $shortcode_attrs[] = 'style="'.$css.'"'; }
	
	return '<blockquote '.implode( " ", $shortcode_attrs ).'>' .do_shortcode( $content ). '</blockquote>';
	
} );

add_shortcode( 'p', function( $atts, $content = null ) {
	
	extract ( shortcode_atts ( array(
		'class' => '',
		'css' => ''
	), $atts ) );
	
	if ( ! empty( $class ) ) {
		$shortcode_attrs[] = 'class="'.$class.'"';
	}
	
	if ( ! empty( $css ) ) {
		$shortcode_attrs[] = 'style="'.$css.'"';
	}
	
	return '<p '.implode( " ", $shortcode_attrs ).'>' .do_shortcode( $content ). '</p>';
	
} );

/**
 * ACCORDIONS
 * 
 */
add_shortcode( 'accordions', function( $atts, $content = null ) {
	
	$GLOBALS[ 'acc_count' ] = 0;
	
	do_shortcode( $content );
	
	$a = shortcode_atts ( array(
		'id' => uniqid()
	), $atts );
	
	if( is_array( $GLOBALS[ 'accs' ] ) ) {
		
		$first = true;
		foreach ( $GLOBALS[ 'accs' ] as $key => $acc ) {
			
			$acc_id = $a[ 'id' ].'-'.$key;
			
			$acc_code =
			'<div class="panel">'.
				'<div class="panel-heading accordion-heading">'.
					'<h4 class="accordion-title">'.
						'<a class="'.( $first ? '' : 'collapsed' ).'" data-toggle="collapse" data-parent="#'.$a[ 'id' ].'" href="#'.$acc_id.'">'
							.do_shortcode( $acc[ 'title' ] ).
						'</a>'.
					'</h4>'.
				'</div>'.
				'<div class="panel-collapse collapse '.( $first ? 'in' : '' ).'" id="'.$acc_id.'">'.
					'<div class="panel-body">'.$acc[ 'content' ].'</div>'.
				'</div>'.
			'</div>';
			
			$accs[] = $acc_code;
			
			$first = false;
		}
		
	}
	
	return '<div class="panel-group accordion" id="'.$a[ 'id' ].'">' .implode ( "", $accs ). '</div>';
	
} );
 
add_shortcode( 'accordion', function( $atts, $content = null ) {
	
 	$a = shortcode_atts ( array(
		'title' => ''
	), $atts );
	
	$acc_count = $GLOBALS[ 'acc_count' ];
	
	$GLOBALS[ 'accs' ][ $acc_count ] = array(
		'title' 	=> $a[ 'title' ],
		'content' 	=> do_shortcode( $content )
	);
		
	$GLOBALS[ 'acc_count' ]++;
	
} );
 
/**
 * TABS
 * 
 */
add_shortcode( 'tabs', function( $atts, $content = null ) {
	
	$GLOBALS[ 'tab_count' ] = 0;
	
	do_shortcode( $content );
	
	$a = shortcode_atts (array (
		'id' 			=> uniqid(),
		'tab_justified'	=> 'false'
	
	), $atts );
	
	if( is_array( $GLOBALS[ 'tabs' ] ) ) {
		
		$first = true;
		foreach ( $GLOBALS[ 'tabs' ] as $key => $tab ) {
			
			$tab_id = $a[ 'id' ].'-'.$key;

			$navs[] = '<li class="'.( $first ? 'active' : '' ).'"><a href="#'.$tab_id.'" data-toggle="tab">'.do_shortcode( $tab[ 'title' ] ).'</a></li>';
			$contents[] = 
			'<div class="tab-pane fade '.( $first ? 'in active' : '' ).'" id="'.$tab_id.'">'
				.$tab[ 'content' ].
			'</div>';
			
			$first = false;
		}
	}
	
	$navs_classes[] = "nav nav-tabs tab-title";
	
	$a[ 'tab_justified' ] = $a[ 'tab_justified' ] === 'true'; 
	if( $a[ 'tab_justified' ] ) { $navs_classes[] = "nav-justified"; }
	 
	$tab_navs = '<ul class="'.implode( " ", $navs_classes ).'">' .implode ( "", $navs ). '</ul>';
	$tab_contents = '<div class="tab-content">' .implode( "", $contents ). '</div>';

	return '<div class="tabs clearfix" id="'.$a[ 'id' ].'">' .$tab_navs.$tab_contents. '</div>';
	
} );
 
add_shortcode( 'tab', function( $atts, $content = null ) {
	
	$a = shortcode_atts ( array(
		'title' => ''
	), $atts );
	
	$tab_count = $GLOBALS[ 'tab_count' ];
	
	$GLOBALS[ 'tabs' ][ $tab_count ] = array(
		'title' 	=> $a[ 'title' ],
		'content' 	=> do_shortcode( $content )
	);
		
	$GLOBALS[ 'tab_count' ]++;
	
} );


/**
 * CAROUSEL
 */
add_shortcode( 'carousel', function( $atts, $content = null ) {
	
	$GLOBALS[ 'carousel_count' ] = 0;
	
	do_shortcode( $content );
	
	$a = shortcode_atts (array (
		'id' 			=> uniqid()
	), $atts );
	
	if( is_array( $GLOBALS[ 'carousel_items' ] ) ) {
		
		$first = true;
		foreach ( $GLOBALS[ 'carousel_items' ] as $key => $item ) {
			
			$indicators[] = '<li data-target="#'.$a[ 'id'].'" data-slide-to="'.$key.'"'.( $first ? ' class="active"' : '' ).'></li>';
			
			$items[] = 
			'<div class="item'.( $first ? ' active' : '' ).'">'.
				'<img src="'.$item[ 'image_src' ].'" />'.
				// ( $item[ 'caption' ] != null ? '<div class="carousel-caption">'.do_shortcode( $item[ 'caption' ] ).'</div>' : '' ).
			'</div>';
			
			$first = false;
			
		}
	}
	
	return 
	'<div id="'.$a[ 'id'].'" class="carousel slide" data-ride="carousel">'.
		'<ol class="carousel-indicators">'.implode( "", $indicators ).'</ol>'.
		'<div class="carousel-inner">'.implode( "", $items ).'</div>'.
		'<a class="left carousel-control" href="#'.$a[ 'id'].'" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>'.
		'<a class="right carousel-control" href="#'.$a[ 'id'].'" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>'.
	'</div>';
	
} );

add_shortcode( 'item', function( $atts, $content = null ) {
	
	$a = shortcode_atts ( array(
		'image_src' => ''
	), $atts );
	
	$carousel_count = $GLOBALS[ 'carousel_count' ];
	
	$GLOBALS[ 'carousel_items' ][ $carousel_count ] = array(
		'image_src'	=> $a[ 'image_src' ]
	);
	
	$GLOBALS[ 'carousel_count' ]++;
	
} );

/**
 * BUTTON
 */
add_shortcode( 'button', function( $atts, $content = null ) {
	
	$a = shortcode_atts ( array (
		'link' => '#',
		'color' => 'default',
		'size' => '',
		
		'tooltip_placement' => 'top',
		'tooltip_title' => '',
	
		'class' => '',
		'css' => '',
	), $atts );
	
} );
