<?php
/**
 * Custom template tags for this theme.
 */

if ( ! function_exists ( 'flirt_comment' ) ) :
/**
 * Template for comments and pingbacks.
 */
function flirt_comment ( $comment, $args, $depth ) {
	
	if ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) : ?>
	
	<? else: ?>
	
	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<article>
			<div class="avatar">
				<?php echo get_avatar( $comment, 45 ); ?>
			</div>
			<div class="comment-body">
				<header class="comment-header">
					<cite class="comment-author">
						<?php 
							printf( __( '%s', 'flirt' ), sprintf( '<cite class="comment-author">%s</cite>', get_comment_author_link() ) );
						?>
					</cite>
					<div class="comment-meta">
						<time pubdate datetime="<?php comment_time( 'c' ); ?>">
							<?php 
								printf( __( '%1$s at %2$s', 'flirt' ), get_comment_date(), get_comment_time() ); 
							?>
						</time>
						<?php
							comment_reply_link( array_merge( $args, array(
								'depth'     => $depth,
								'max_depth' => $args[ 'max_depth' ],
								'before'    => '<span class="reply-link">',
								'after'     => '</span>',
							) ) );
						?>
					</div>
				</header>
				<div class="comment-content">
					<?php if ( $comment->comment_approved == '0' ) : ?>
					<small><?php _e( 'This comment is awaiting moderation', 'flirt' ); ?></small>
					<?php endif; ?>
					
					<?php comment_text(); ?>
				</div>
			</div>
			
		</article>
	</li>
	
	<? endif; 
}
endif;

if ( ! function_exists ( 'flirt_content_nav' ) ) :

function flirt_content_nav ( $nav_id ) {
	global $wp_query, $post;
		
	// Don't print empty markup on single pages if there's nowhere to navigate.
	if ( is_single() ) {
		$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
		$next = get_adjacent_post( false, '', false );
		
		if ( ! $next && ! $previous )
		return;
	}
	
	// Don't print empty markup in archives if there's only one page.
	if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
		return;
		
	?>
	
	<?php if ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : // navigation links for home, archive, and search pages ?>
	<nav class="pagination">	
		<?php
		    $wp_query;
		    $big = 99999999;
		    echo paginate_links(array(
		        'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
		        'format' => '?paged=%#%',
		        'total' => $wp_query->max_num_pages,
		        'current' => max( 1, get_query_var( 'paged' ) ),
		        'show_all' => false,
		        'end_size' => 2,
		        'mid_size' => 3,
		        'prev_next' => true,
		        'prev_text' => 'Prev',
		        'next_text' => 'Next',
		        'type' => 'plain'
		    ));
		?>
	</nav>	
	<?php endif; ?>
	
	<?php
}

endif;

if ( ! function_exists ( 'flirt_pagination' ) ) :
/**
 * Template for custom wp_link_pages
 */
function flirt_pagination ( $args = '' ) {
	
	$defaults = array(
		'before' => '<nav class="pagination">', 
		'after' => '</nav>',
		'text_before' => '',
		'text_after' => '',
		'next_or_number' => 'number', 
		'nextpagelink' => __( 'Next' ),
		'previouspagelink' => __( 'Prev' ),
		'pagelink' => '%',
		'echo' => 1
	);
	
	$r = wp_parse_args( $args, $defaults );
	$r = apply_filters( 'wp_link_pages_args', $r );
	extract( $r, EXTR_SKIP );

	global $page, $numpages, $multipage, $more, $pagenow;

	$output = '';
	if ( $multipage ) {
		if ( 'number' == $next_or_number ) {
			$output .= $before;
			for ( $i = 1; $i < ( $numpages + 1 ); $i = $i + 1 ) {
				$j = str_replace( '%', $i, $pagelink );
				$output .= ' ';
				if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
					$output .= _wp_link_page( $i );
				else
					$output .= '<span class="current">';

				$output .= $text_before . $j . $text_after;
				if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
					$output .= '</a>';
				else
					$output .= '</span>';
			}
			$output .= $after;
		} else {
			if ( $more ) {
				$output .= $before;
				$i = $page - 1;
				if ( $i && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $text_before . $previouspagelink . $text_after . '</a>';
				}
				$i = $page + 1;
				if ( $i <= $numpages && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $text_before . $nextpagelink . $text_after . '</a>';
				}
				$output .= $after;
			}
		}
	}

	if ( $echo ) {
		echo $output;
	}

	return $output;
	
}
endif;

if ( ! function_exists ( 'flirt_breadcrumb' ) ) :

function flirt_breadcrumb () {
	
	echo '<ul class="breadcrumb pull-right">';
	
	global $post;
	
	if ( ! is_home() || ! is_front_page() ) {
		echo '<span>You are in: &nbsp;</span>';
		echo '<li><a href="',get_option( 'home' ),'">Home</a></li>';
		
		if ( is_category() || is_single() ) {
			
			echo '<li>',the_category(', '),'</li>';
			
            if ( is_single() ) {
                echo '<li>',the_title(),'</li>';
            }
			
		} elseif ( is_page() ) {
			
			if ( $post -> post_parent ) {
				$ancs = get_post_ancestors( $post->ID );
				$title = get_the_title();
				
				foreach ( array_reverse( $ancs ) as $anc ) {
					 echo '<li><a href="'.get_permalink( $anc ).'">'.get_the_title( $anc ).'</a></li>';
				}
				echo '<li>'.$title.'</li>';

			} else {
				echo '<li>'.get_the_title().'</li>';
			}
			
		} elseif ( is_search() ) { echo '<li>Search Result</li>';
		} elseif ( is_tag() ) { echo '<li>',single_tag_title(),'</li>'; 
		} elseif ( is_day() ) { echo '<li>Archive for by Date</li>';
		} elseif ( is_month() ) { echo '<li>Archive by Month</li>';
		} elseif ( is_year() ) { echo '<li>Archive by Year</li>';
		} elseif ( is_author() ) { echo '<li>Archive by Author</li>'; 
		}
		
	}
	
	echo '</ul>';
}

endif;

if ( ! function_exists ( 'flirt_post_detail' ) ) :
function flirt_post_detail () {
	$categories_list = get_the_category_list( __( ', ', 'flirt' ) );
		
	$date = sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a>',
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);
	
	$author = sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( __( 'View all posts by %s', 'flirt' ), get_the_author() ) ),
		get_the_author()
	);
	
	$comments_count = get_comments_number();

	printf (
	__( '<ul class="post-detail">'.
			'<li class="post-date"><i class="icon-calendar"></i>%2$s</li>'.
			'<li class="post-author"><i class="icon-pencil"></i>%3$s</li>'.
			'<li class="post-comments"><i class="icon-comments-alt"></i>%4$s Comments</li>'.
			'<li class="post-category"><i class="icon-folder-open-alt"></i>%1$s</li>'.
		'</ul>', 'flirt' ),
		$categories_list, $date, $author, $comments_count 
	);
}
endif;

if ( ! function_exists ( 'flirt_post_tags' ) ) :
function flirt_post_tags () {
	$tags_list = get_the_tag_list('<ul class="post-tags"><li>','</li><li>','</li></ul>');
	
	printf( __( '%1$s', 'flirt' ), $tags_list );
}
endif;
