<?php
/**
 * The main template file.
 *
 * @package flirt
 */
get_header(); ?>

<div id="site-content">
	<div class="container">
		<div class="row clearfix">
			
			<div class="col-md-9">
				
				<?php if ( have_posts() ) : ?>
					
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>
						
						<?php 
							if ( ! get_post_format() ) :
								get_template_part( 'format', 'standard' );
								
							else:
								get_template_part( 'format', get_post_format() );
								
							endif;
						?>
						<?php
							
							// $media_url = get_post_meta( $post->ID, 'media-url', true );
							// $media_embedded = get_post_meta( $post->ID, 'media-embedded', true );
							
							// echo ( $media_url ? $media_url : 'no-url' );
							// echo "<br/>";
							// echo ( $media_embedded ? $media_embedded : 'no-embed' );
							// echo "<br/>";
							
							// if ( $media_url ) {
								// echo "yes<br/>";
								// $embed_code = wp_oembed_get( $media_url );
								// echo $embed_code;
							// }
							
						?>
						
					<?php endwhile; ?>
					
					<?php flirt_content_nav(); ?>
					
				<?php endif; ?>
				
			</div>
			
			<?php get_sidebar(); ?>
			
		</div>
		
	</div>
</div>

<?php get_footer(); ?>
