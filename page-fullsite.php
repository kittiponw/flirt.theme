<?php
/**
 * Template Name: Full-site ( no sidebar & full screen width )
 * 
 * This is the template that displays full screen width page without sidebar
 * 
 * @package flirt
 */

get_header(); ?>

<div id="site-content">
				
	<?php while ( have_posts() ) : the_post(); ?>
		
		<?php the_content(); ?>
		<?php flirt_content_nav(); ?>
		
	<?php endwhile; ?>
					
</div>

<?php get_footer(); ?>