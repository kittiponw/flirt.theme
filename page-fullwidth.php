<?php
/**
 * Template Name: Full-width ( no sidebar )
 * 
 * This is the template that displays full width page without sidebar
 * 
 * @package flirt
 */

get_header(); ?>

<div id="site-content">
	<div class="container">
		<div class="row clearfix">
			
			<div class="col-lg-12">
				
				<?php while ( have_posts() ) : the_post(); ?>
					
					<?php get_template_part( 'content', 'page' ); ?>
					<?php flirt_content_nav(); ?>
					
				<?php endwhile; ?>
					
			</div>
			
		</div>
	</div>
</div>

<?php get_footer(); ?>