<?php
/**
 * The template for displaying all pages.
 * 
 * @package flirt
 */

get_header(); ?>

<div id="site-content">
	<div class="container">
		<div class="row clearfix">
			
			<div class="col-md-9">
				
				<?php while ( have_posts() ) : the_post(); ?>
					
					<?php get_template_part( 'content', 'page' ); ?>
					<?php flirt_content_nav(); ?>
					
				<?php endwhile; ?>
					
			</div>
			
			<?php get_sidebar(); ?>
			
		</div>
		
	</div>
</div>

<?php get_footer(); ?>