<?php
/**
 * @package flirt
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( "blog-post" ); ?>>
							
	<header class="post-header">
		<h1 class="post-header-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
	</header>
	
	<?php 
		$audio_url = get_post_meta( $post->ID, 'audio-url', true ); 
		$embedded = get_post_meta( $post->ID, 'audio-embedded', true );
	?>
	 
	<?php if ( $audio_url ) : ?>
	<div class="post-media">
		<div class="media">
		<?php echo wp_oembed_get( $audio_url ); ?>
		</div>
	</div>
		
	<?php endif; // $post_media ?>
	
	<div class="row post-body">
		<div class="col-sm-8 col-sm-push-4">
			
			<div class="post-content">
				<?php the_content( __( 'Read more..', 'flirt' ) ); ?>
			</div>
					
		</div>
		<div class="col-sm-4 col-sm-pull-8">
			
			<?php flirt_post_detail(); ?>
			
			<?php flirt_post_tags(); ?>
			
		</div>
	</div>
	
</article>