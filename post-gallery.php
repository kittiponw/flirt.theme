<?php
/**
 * @package flirt
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( "blog-post" ); ?>>
							
	<header class="post-header">
		<h1 class="post-header-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
	</header>
	
	<?php $image_ids = get_post_meta( $post->ID, 'image-ids', true ); ?>
				 
	<?php if ( $image_ids ) : ?>
	<div class="post-media">
		<div id="gallery-<?php the_ID(); ?>" class="carousel slide" data-ride="carousel">
			
			<div class="carousel-inner">
				<?php $first = true; ?>
				<?php foreach( explode( ",", $image_ids ) as $img_id ) : ?>
				<div class="item<?php echo ( $first ? ' active' : '' ) ?>">
					<img src="<?php echo wp_get_attachment_url( $img_id ); ?>"/>
				</div>
				<?php $first = false; ?>
				<?php endforeach; ?>
			</div>
			
			<a class="left carousel-control" href="#gallery-<?php the_ID(); ?>" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#gallery-<?php the_ID(); ?>" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			
		</div>
	</div>
	<?php endif; // $post_media ?>
	
	<div class="row post-body">
		<div class="col-sm-8 col-sm-push-4">
			
			<div class="post-content">
				<?php the_content( __( 'Read more..', 'flirt' ) ); ?>
			</div>
					
		</div>
		<div class="col-sm-4 col-sm-pull-8">
			
			<?php flirt_post_detail(); ?>
			
			<?php flirt_post_tags(); ?>
			
		</div>
	</div>
	
</article>