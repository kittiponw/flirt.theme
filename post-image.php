<?php
/**
 * @package flirt
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( "blog-post" ); ?>>
	
	<header class="post-header">
		<h1 class="post-header-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
	</header>
	
	<?php $image_id = get_post_meta( $post->ID, 'image-id', true ); ?>
				 
	<?php if ( $image_id ) : ?>
	<div class="post-media">
		<img src="<?php echo wp_get_attachment_url( $image_id ); ?>" class="img-responsive"/>
	</div>
	<?php endif; // $post_media ?>
	
	<div class="row post-body">
		<div class="col-sm-8 col-sm-push-4">
			
			<div class="post-content">
				<?php the_content( __( 'Read more..', 'flirt' ) ); ?>
			</div>
			
		</div>
		<div class="col-sm-4 col-sm-pull-8">
			
			<?php flirt_post_detail(); ?>
			
			<?php flirt_post_tags(); ?>
			
		</div>
	</div>
	
</article>