<?php
/**
 * Replacing the default WordPress search form
 * 
 * @package FiRST
 */
?>
<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url ( '/' ) ); ?>">
	<input type="text" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" id="s" placeholder="<?php  echo __("SEARCH TERM..."); ?>" />
</form>