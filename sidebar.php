<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package flirt
 */
?>
<div class="col-md-3" role="complementary">
	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	<?php endif; ?>
</div>
