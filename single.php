<?php 
/**
 * The template for displaying all single posts.
 * 
 * @package flirt
 */
get_header(); ?>

<div id="site-content">
	<div class="container">
		<div class="row clearfix">
			
			<!-- MAIN AREA -->
			<div class="col-md-9">
				
				<?php while ( have_posts() ) : the_post(); ?>
					
					<?php 
						if ( ! get_post_format() ) :
							get_template_part( 'post', 'standard' );
							
						else:
							get_template_part( 'post', get_post_format() );
							
						endif;
					?>
					
					<?php flirt_pagination(); ?>
					
					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() )
							comments_template( '', true );
							
					?>
				<?php endwhile; ?>
				
			</div>
			<!-- MAIN AREA -->
			
			<?php get_sidebar(); ?>
			
		</div>
		
	</div>
</div>

<?php get_footer(); ?>